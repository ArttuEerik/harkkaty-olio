
package harkkatyo;


import java.io.PrintWriter;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class Harkkatyo extends Application {
    private logFile file;
    private PrintWriter writer;
    
    
    @Override
    public void start(Stage stage) throws Exception {
        file = logFile.getInstance();
        writer = new PrintWriter("Kuitti","UTF-8");
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        
        stage.show();
        //Making hanlde for executin following methon when user is closing this program
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            //Writing log into file when closing program
        @Override
        public void run() {
            writer.write("Lähetettyjä paketteja: " + file.getSendSaldo()+"\n"+
                        "Varastossa olevia pakettaja: " + file.getWareSaldo()+ "\n\n" + file.getContent() + file.getStuff());
            
            writer.close();
        }
    }, "Shutdown-thread"));
        
    }
}

   
      
        
        
        
        
    
    

