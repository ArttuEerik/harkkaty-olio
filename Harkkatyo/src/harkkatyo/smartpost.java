
package harkkatyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class smartpost {
    
    private String code;
    private String city;
    private String address;
    private String availab;
    private String office;
    private geoData location;

    
    
    public smartpost(String c, String C, String add, String av, String off, float la, float ln) {
        //Add values
        this.code = c;
        this.city = C;
        this.address = add;
        this.availab = av;
        this.office = off;
        //set lucation values
        location = new geoData(la, ln);
    }

    // sublass for location data
    public class geoData {
        private float lat;
        private float lng;
        
        public geoData(float la, float ln) {
            this.lat = la;
            this.lng = ln;
        }
        
        public float getLat() {
            return lat;
        }

        public float getLng() {
            return lng;
        }
    }
    
    @Override
    public String toString() {
        return city + ", " + office;
    }

    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailab() {
        return availab;
    }

    public String getOffice() {
        return office;
    }
    
    public geoData getLocation() {
        return location;
    }
    
}

class parseOffices {
    private ArrayList<smartpost> offices;
    private ArrayList<String> clusters;
    private Document doc;
    private String content;
    private String line;

    
    parseOffices() throws SAXException, IOException {
        //reading data from web
        URL url = new URL("http://smartpost.ee/fi_apt.xml");
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        content = "";
        while((line = br.readLine()) != null) {
            content += line +"\n";
        }
        br.close();
        
        //parsing data to 'smartpost' elements
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();        
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            doc = db.parse(new InputSource(new StringReader(content)));
            offices = new ArrayList();
            
            NodeList nodes = doc.getElementsByTagName("place");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) node;
                    //getting all information from every smartpost
                    String c = e.getElementsByTagName("code").item(0).getTextContent();
                    String C = e.getElementsByTagName("city").item(0).getTextContent();
                    String add = e.getElementsByTagName("address").item(0).getTextContent();
                    String av = e.getElementsByTagName("availability").item(0).getTextContent();
                    String off = e.getElementsByTagName("postoffice").item(0).getTextContent();
                    Float la = Float.parseFloat(e.getElementsByTagName("lat").item(0).getTextContent());
                    Float ln = Float.parseFloat(e.getElementsByTagName("lng").item(0).getTextContent());
                    
                    //creating new smartpost to list
                    offices.add(new smartpost(c, C, add, av ,off, la, ln));  
                    
                }                
            }
            
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(parseOffices.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Creating city list (clusters)
        clusters = new ArrayList();
        for (smartpost office : offices) {
            if (!clusters.contains(office.getCity())) {
                clusters.add(office.getCity());
            }
        }
    } 
    
    public ArrayList<smartpost> getList() {
        return offices;
    }
    
    public ArrayList<String> getClusters() {
        return clusters;
    }
}