
package harkkatyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;


public class LogScreenController implements Initializable {
    private logFile log;
    
    @FXML
    private TextArea logScreen;
    @FXML
    private Label craftedPackages;
    @FXML
    private Label sendPackages;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            //showing log data at new window
            log = logFile.getInstance();
            craftedPackages.setText(log.getWareSaldo());
            sendPackages.setText(log.getSendSaldo());
            
            logScreen.setText(log.getContent() + log.getStuff());
            
    }

        
        
}    
    

