
package harkkatyo;
//Class creates items for packages
abstract class thing{
    // values for items
    protected String Luokka;
    protected String name;
    protected float korkeus;
    protected float leveys;
    protected float syvyys;
    protected float paino;
    protected boolean broke;
        
    
    @Override
    public String toString() {
        return name;
    }

    public String getLuokka(){
        return Luokka;
    }
    public String getName() {
        return name;
    }
    public float getKorkeus() {
        return korkeus;
    }
    public float getLeveys() {
        return leveys;
    }
    public float getSyvyys() {
        return syvyys;
    }
    public float getPaino() {
        return paino;
    }
    public boolean isBroke() {
        return broke;
    }    
}    

//Classes for standard items
class Timo extends thing{
   Timo(){
     Luokka = "Luokka 3";
     name = "Timo";
     korkeus = 185f;
     leveys = 50f;
     syvyys = 35f;
     paino = 88f;
     broke = true;
    }    
}
class Doge extends thing{
    Doge(){
      Luokka = "Luokka 3";
      name = "Doge";
      korkeus = 50f;
      leveys = 30f;
      syvyys = 20f;
      paino = 30f;
      broke = false;
    }
    
}
class Lasimaljakko extends thing{
    Lasimaljakko(){
      Luokka = "Luokka 3";
      name = "Lasimaljakko";
      korkeus = 30f;
      leveys = 7f;
      syvyys = 7f;
      paino = 3f;
      broke = true;
    }
}
class Tieskari extends thing{
    Tieskari(){
      Luokka = "Luokka 3";
      name = "Tieskari";
      korkeus = 40f;
      leveys = 55f;
      syvyys = 7f;
      paino = 12f; 
      broke = true;
    }

    
}
//User-defined item creation
class Object extends thing{
    Object(String Value,String nimi,float lenght,float withd,float depht,float mass,boolean bool){
      Luokka = Value;
      name = nimi;
      korkeus = lenght;
      leveys = withd;
      syvyys = depht;
      paino = mass; 
      broke = bool;
        
    }
}
