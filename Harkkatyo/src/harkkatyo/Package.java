
package harkkatyo;

import java.util.ArrayList;

public abstract class Package {
    //needed variables
    protected String start;
    protected String end;
    protected int priority;
    protected float pituus;
    protected float leveys;
    protected float syvyys;
    protected String id;
    protected float paino;
    protected String sisalto;
    protected  float startLat;
    protected  float startLng;
    protected  float endLat;
    protected  float endLng;
    protected boolean bool;
    protected float distance;
    
    @Override
    public String toString() {
        return id;
    }
    public String getStart(){
        return start;
    }
    public String getEnd(){
        return end;
    }
    
    public int getPrio() {
        return priority;
    }

    public float getPituus() {
        return pituus;
    }
    
    public float getLeveys() {
        return leveys;
    }

    public float getSyvyys() {
        return syvyys;
    }

    public String getId() {
        return id;
    }

    public float getPaino() {
        return paino;
    }

    public boolean getBoolean(){
        return bool;
    }
    public float getDistance(){
        return distance;
    }
    
    
}
//Different package classes are made here
class package1 extends Package{
    package1(float lenght,float withd,float depth, String name,float weight, float S1, float S2, float E1, float E2,boolean parameter,String a,String l,float trip){
        priority = 1;
        pituus = lenght;
        leveys = withd;
        syvyys = depth;
        id = name;
        paino = weight;
        startLat = S1;
        startLng = S2;
        endLat = E1;
        endLng = E2;
        bool = parameter;
        start = a;
        end = l;
        distance = Math.round(trip/1000);
    }
}
class package2 extends Package{
    package2(float lenght,float withd,float depth, String name,float weight, float S1, float S2, float E1, float E2,boolean parameter,String a, String l,float trip){
        priority = 2;
        pituus = lenght;
        leveys = withd;
        syvyys = depth;
        id = name;
        paino = weight;
        startLat = S1;
        startLng = S2;
        endLat = E1;
        endLng = E2;
        bool = parameter;
        start = a;
        end = l;
        distance = Math.round(trip/1000);
    }
}
class package3 extends Package{
    package3(float lenght,float withd,float depth, String name,float weight, float S1, float S2, float E1, float E2,boolean parameter,String a,String l,float trip){
        priority = 3;
        pituus = lenght;
        leveys = withd;
        syvyys = depth;
        id = name;
        paino = weight;
        startLat = S1;
        startLng = S2;
        endLat = E1;
        endLng = E2;
        bool = parameter;
        start = a;
        end = l;
        distance = Math.round(trip/1000);
    }
}

class warehouse {
//This is storage class for packages    
    private ArrayList<Package> storage;
    private ArrayList<smartpost> offs;
  
    //warehouse needs to be singleton
    static private warehouse house = null;
    
    private warehouse() {
        //initializing arrayLists
        storage = new ArrayList();
        offs = new ArrayList();
        
    }
    
    static public warehouse getInstance() {
        if(house == null) {
            house = new warehouse();
        }
        return house;
    }
    //method checks witch package class sould be created and makes new package to storage    
    public String creation(String clas,thing paketti, float S1, float S2, float E1, float E2,boolean parameter,String start, String end) {
        
        //creation command for packages
        if("Luokka 1".equals(clas)) {

            //check if distance <= 150 km
            if(distance(S1, S2, E1, E2) <= 150000 ) {
                getStorage().add(new package1(paketti.getKorkeus(), paketti.getLeveys(), paketti.getSyvyys(), paketti.getName(), paketti.getPaino(), S1, S2, E1, E2,parameter, start,end,distance(S1, S2, E1, E2)));
            }   
            else return "Matka liian pitkä luokan 1 lähetykselle!";
        }
        if("Luokka 2".equals(clas)) {
            //check if size is small enough
            if(paketti.getKorkeus() > 30 || paketti.getLeveys() > 30 || paketti.getSyvyys() > 30) {
                return "Esineen mitat ovat liian suuret luokan 2 lähetykselle!";
            }
            else {
            getStorage().add(new package2(paketti.getKorkeus(), paketti.getLeveys(), paketti.getSyvyys(), paketti.getName(),paketti.getPaino(), S1, S2, E1, E2,parameter, start, end,distance(S1, S2, E1, E2)));
            }
        }
        if("Luokka 3".equals(clas)) { 
            //accepts all packages
            getStorage().add(new package3(paketti.getKorkeus(), paketti.getLeveys(), paketti.getSyvyys(), paketti.getName(),paketti.getPaino(), S1, S2, E1, E2,parameter, start, end,distance(S1, S2, E1, E2)));
        }
        
        return "Paketti lisätty varastoon.";
    }
    protected float distance(float SLat, float SLng, float ELat, float ELng) {
        //method for calculating distane (this was found online)
        double earthRadius = 6371000;
        double LatD = Math.toRadians(ELat - SLat);
        double LngD = Math.toRadians(ELng - SLng);
        double a = Math.sin(LatD / 2) * Math.sin(LatD / 2) +
                   Math.cos(Math.toRadians(SLat)) * Math.cos(Math.toRadians(ELat)) *
                   Math.sin(LngD /2) * Math.sin(LngD / 2);
        double c = 2 * Math.atan2((Math.sqrt(a)), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);
        
        return dist;
    }

    
    public ArrayList<Package> getStorage() {
        return storage;
    }
    
    //remove method used when package is sent
    public void del(Package P) {
        storage.remove(P);
    }
    
    public void setOff(smartpost off) {
        this.offs.add(off);
    }
    
    public ArrayList<smartpost> getOffs() {
        return offs;
    }
   
    
}


 



    
