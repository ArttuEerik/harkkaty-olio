
package harkkatyo;

import static java.lang.Float.parseFloat;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


public class ScreenController implements Initializable {
    //introducing classes needed here
    private warehouse W;
    private ArrayList<String> Class;

    
    @FXML
    private TextField namefield;
    @FXML
    private TextField lenghtfield;
    @FXML
    private TextField massfield;
    @FXML
    private TextField widhtfield;
    @FXML
    private TextField dephtfield;
    @FXML
    private ComboBox<String> classBox;
    @FXML
    private Button createButton;
    @FXML
    private Label infoLabel;
    @FXML
    private Label classLabel;
    @FXML
    private ComboBox<smartpost> startBox;
    @FXML
    private ComboBox<smartpost> targetBox;
    @FXML
    private CheckBox isFragile;
    @FXML
    private ComboBox<String> Goods;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Getting our warehouse for later usage
        W = warehouse.getInstance();
        //Adding default item names to ComboBox
        Goods.getItems().add("Timo");
        Goods.getItems().add("Doge");
        Goods.getItems().add("Lasimaljakko");
        Goods.getItems().add("Tieskari");
        //Set everything to default
        Class = new ArrayList();
        Class.add("Luokka 1");
        Class.add("Luokka 2");
        Class.add("Luokka 3");
        classBox.getItems().addAll(Class);
        classLabel.setText("Valitse lähetysluokka listasta \nsaadaksesi lisätietoa.");
        
        //Arraylist for drawn locations
        startBox.getItems().addAll(W.getOffs());
        targetBox.getItems().addAll(W.getOffs());
    }    

    @FXML
    private void setInfoText(ActionEvent event) {
        // method for informing user about different package classes
        if("Luokka 1".equals(classBox.getValue())){
            classLabel.setText("Luokan 1 paketilla voit lähettää nopeasti esineitä.\nPaketilla et voi lähettää yli 150km lähetyksiä.");
        }
        if("Luokka 2".equals(classBox.getValue())) {
            classLabel.setText("Turvalähetysvaihtoehto:\nesineet eivät hajoa tässä paketissa.\nKoko on rajoitettu 30x30x30 cm.");
        }
        if("Luokka 3".equals(classBox.getValue())) {
            classLabel.setText("Suositellaan painaville lähetyksille.\nYli 80kg painoiset lähetykset eivät hajoa.");
        }
    }
    
    @FXML
    private void create(ActionEvent event) {
        /*
        'create' uses boolean methods 'mailData', 'createItem', 'hasNoValue' and 'initFields'
        to simplify the code and checking user errors
        */ 
        
        //check if mailing data(locations, package class) are chosen
        if(!mailData()) {
            infoLabel.setText("Paketin luonti epäonnistui.\nAseta paketille lähtöpiste ja määränpää\nsekä sopiva pakettiluokka!");
        }
        else {
            //check if package is cosen from item list -> creation
            if(createItem()) {
                initFields(); //method updates/clears all required areas; see method below
            }
            //if item is not chosen -> check if all fields are filled for cretion of new item
            else if(hasNoValue()) {
                infoLabel.setText("Paketin luonti epäonnistui.\nValitse esine listalta tai tee oma.\nKaikki kentät tulee täyttää.");
            }
            //new item is creted here
            else {
                //Creation of new package and setting info message to label
                infoLabel.setText(W.creation(classBox.getValue(), new Object(
                                                                    classBox.getValue(), 
                                                                    namefield.getText(), 
                                                                    parseFloat(lenghtfield.getText()), 
                                                                    parseFloat(widhtfield.getText()), 
                                                                    parseFloat(dephtfield.getText()), 
                                                                    parseFloat(massfield.getText()),
                                                                    true),
                                    
                                    startBox.getValue().getLocation().getLat(), startBox.getValue().getLocation().getLng(),
                                    targetBox.getValue().getLocation().getLat(), targetBox.getValue().getLocation().getLng(),
                                    isFragile.isSelected(), startBox.getValue().getOffice(),targetBox.getValue().getOffice()));
                initFields();
            }
        }
    }

    private boolean createItem() {
        //check if an item is selected
        if(Goods.getValue() == null){
            return false;
        }
        //Creating all default goods
        switch (Goods.getValue()){
            case "Timo":
                // Timo allways goes to university and travels by package class 3
                infoLabel.setText(W.creation("Luokka 3", new Timo(), startBox.getValue().getLocation().getLat(),startBox.getValue().getLocation().getLng(),
                        61.065895f, 28.09375f, false,startBox.getValue().getOffice(), "Pakettiautomaatti, Lappeenrannan teknillinen yliopisto") +
                        "\nTimo kulkee aina luokan 3 paketeilla.\n(Jos valitsit muun paketin, ei valitaa huomioitu)" );
                
                return true;
            case "Doge":
                infoLabel.setText(W.creation(classBox.getValue(), new Doge(),
                        startBox.getValue().getLocation().getLat(),startBox.getValue().getLocation().getLng(),
                        targetBox.getValue().getLocation().getLat(),targetBox.getValue().getLocation().getLng(),false,startBox.getValue().getOffice(),targetBox.getValue().getOffice()));
                return true;
            case "Lasimaljakko":
                infoLabel.setText(W.creation(classBox.getValue(), new Lasimaljakko(),
                        startBox.getValue().getLocation().getLat(),startBox.getValue().getLocation().getLng(),
                        targetBox.getValue().getLocation().getLat(),targetBox.getValue().getLocation().getLng(),true,startBox.getValue().getOffice(),targetBox.getValue().getOffice()));
                return true;
            case "Tieskari":
                infoLabel.setText(W.creation(classBox.getValue(), new Tieskari(),
                        startBox.getValue().getLocation().getLat(),startBox.getValue().getLocation().getLng(),
                        targetBox.getValue().getLocation().getLat(),targetBox.getValue().getLocation().getLng(),true,startBox.getValue().getOffice(),targetBox.getValue().getOffice()));
                return true;
        }
        return false;
    }
    
    private boolean hasNoValue() {
        //simply method for checking if all fields are filled
        if(namefield.getText().isEmpty()){
            return true;
        }
        if(lenghtfield.getText().isEmpty()){
            return true;
        }
        if(widhtfield.getText().isEmpty()){
            return true;
        }
        if(dephtfield.getText().isEmpty()){
            return true;
        }
        return massfield.getText().isEmpty();
    }
    private boolean mailData() {
        //simply check for combobox selections: needed data for creating packages
        if(classBox.getValue() == null){
            return false;
        }
        if(startBox.getValue() == null){
            return false;
        }
        return targetBox.getValue() != null;
    }
    
    private void initFields() {
        
        //clearing commands for textfields and class selector
            namefield.clear();
            lenghtfield.clear();
            massfield.clear();
            widhtfield.clear();
            dephtfield.clear();
            isFragile.setSelected(false);
            classBox.setValue(null);
            Goods.setValue(null);
            classLabel.setText("Valitse lähetysluokka listasta \nsaadaksesi lisätietoa.");

            //updating comboBoxes for sending packages
            startBox.getItems().clear();
            targetBox.getItems().clear();
            startBox.getItems().addAll(W.getOffs());
            targetBox.getItems().addAll(W.getOffs());
    
    }

}
