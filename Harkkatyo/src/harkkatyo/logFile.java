
package harkkatyo;

//logFile has to be singleton
//Storing log info about storage and send packages
class logFile {
    //info about send packages
    private String content;
    
    //info about storages balance
    private String stuff;
    
    //info about already crafted packages that are not send
    private int wareSaldo;
    
    //info about send packages
    private int sendSaldo;

    static private logFile file = null;
    
    private logFile(){
        stuff = "";
        content = "";
    }
    
    static public logFile getInstance(){
        if(file == null){
            file = new logFile();
        }
        return file;
    }
    
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String k) {
        this.content = k;
    }

    /**
     * @return the stuff
     */
    public String getStuff() {
        return stuff;
    }

    /**
     * @param stuff the stuff to set
     */
    public void setStuff(String f) {
        this.stuff = f;
    }

    /**
     * @return the wareSaldo
     */
    //change parameter type for GUI
    public String getWareSaldo() {
        return Integer.toString(wareSaldo);
    }

    /**
     * @param wareSaldo the wareSaldo to set
     */
    public void setWareSaldo(int wareSaldo) {
        this.wareSaldo = wareSaldo;
    }

    /**
     * @return the sendSaldo
     */
    //change parameter type for GUI
    public String getSendSaldo() {
        return Integer.toString(sendSaldo);
    }

    /**
     * @param sendSaldo the sendSaldo to set
     */
    public void setSendSaldo(int sendSaldo) {
        this.sendSaldo = sendSaldo;
    }
}
