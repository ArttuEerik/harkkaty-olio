
package harkkatyo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.xml.sax.SAXException;


public class FXMLDocumentController implements Initializable {
    private parseOffices P;
    private warehouse W;
    private logFile log;
    
    private String colour;
    private String speed;
    private String line = "";
    private String word = "";
    private int calculator;
    private int number;
    private ArrayList<Package> list;
    
    @FXML
    private WebView webPage;
    @FXML
    private ComboBox<smartpost> Dogebox;
    @FXML
    private Button addPost;
    @FXML
    private Button roadButton;
    @FXML
    private Button removeButton;
    @FXML
    private Button packageButton;
    @FXML
     private ComboBox<Package> packageBox;
    @FXML
    private ComboBox<String> cityBox;
    @FXML
    private Label frontLabel;
    @FXML
    private Button openLogButton;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        log = logFile.getInstance();
        W = warehouse.getInstance();
        list = new ArrayList();
        
        
        //Map to WebView
        webPage.getEngine().load(getClass().getResource("index.html").toString());
        
        //Creation of new 'parseOffices' element from 'smartposts' data
        try {
            P = new parseOffices();
            cityBox.getItems().addAll(P.getClusters());
            
        } catch (SAXException | IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    private void findByCity(ActionEvent event) {
        
        //search based by city
        Dogebox.getItems().clear();
        for(int i = 0; i < P.getList().size(); i++) {
            
            //Dogebox has only offices from city chosen in cityBox
            if(P.getList().get(i).getCity().equals(cityBox.getValue())) {
                Dogebox.getItems().add(P.getList().get(i));
            }
        }
    }
    @FXML
    private void addOffice(ActionEvent event) {
        
        //check if office is selected
        if(Dogebox.getValue() == null) {
            frontLabel.setText("Et ole valinnut lisättävää Dogepostia!");
        }
        else {
            
            //add new location to list of drawn items
            //Cant use post code in address because some of the code are differen than gmaps says they are
            W.setOff(Dogebox.getValue());
            String adress = Dogebox.getValue().getAddress() + ","+ Dogebox.getValue().getCity();
            String misc = Dogebox.getValue().toString() + ", Auki: " + Dogebox.getValue().getAvailab();
            String colour = "red";
            String location = "document.goToLocation("+ "'" + adress + "', '" + misc + "', '" + colour + "'" + ")";
            webPage.getEngine().executeScript(location); 
            frontLabel.setText("Dogeposti lisätty.");
            }
    }

    @FXML
    private void drawRoad(ActionEvent event) throws FileNotFoundException, UnsupportedEncodingException  {
        //check if package is selected
        
        if(packageBox.getValue() == null) {
            frontLabel.setText("Ei pakettia valittu!");
        }
        else {
            //Updating logfile list for send packages
            list.add(packageBox.getValue());
            log.setContent(writePaper());
            
            // cordinates for delivery: start point is red from startBox and ending from endBox
            ArrayList<Float> route = new ArrayList();
            route.add(packageBox.getValue().startLat);
            route.add(packageBox.getValue().startLng);
            route.add(packageBox.getValue().endLat);
            route.add(packageBox.getValue().endLng);
            
            if(packageBox.getValue().getPrio() == 1) {
            //colour is changed based of package
            colour = "red";
            //speed of delivery 
            speed = "1";
            }
            if(packageBox.getValue().getPrio() == 2) {
            colour = "blue";
            speed = "2";
            }
            if(packageBox.getValue().getPrio() == 3) {
            colour = "DarkViolet";
            speed = "3";
            }
            
            //javascript command is made here to siplify the code
            String road = "document.createPath(" + route + ", '" + colour + "', '" + speed + "')";
            webPage.getEngine().executeScript(road);
            if(packageBox.getValue().getId().equals("Timo")){
                frontLabel.setText("Timo vaeltaa aina takaisin yliopistolle!!");
            }
            else{
                if(packageBox.getValue().getClass().getSimpleName().equals("package2")){
                    frontLabel.setText("Paketti saapui ehjänä perille.");
                }
                else{
                    if(packageBox.getValue().getClass().getSimpleName().equals("package3")
                       && packageBox.getValue().getPaino() > 80 ) {
                            frontLabel.setText("Paketti saapui ehjänä perille.");
                    }
                    else if(packageBox.getValue().getBoolean() == true){
                        frontLabel.setText("Paketin sisältö särkyi matkalla.");
                    }
                    else{
                        frontLabel.setText("Paketti saapui ehjänä perille.");
                    }
                }
            }
            
            //sent package needs to be deleted
            W.del(packageBox.getValue());
            //interface update for storage
            packageBox.getItems().clear();
            packageBox.getItems().addAll(W.getStorage());
            
            //updating strorages log info
            log.setStuff(getStuffFromStorage());
        }
    }

    @FXML
    private void deleteRoad(ActionEvent event) {
        //deletes all drawn routes on map
        webPage.getEngine().executeScript("document.deletePaths()"); 
        frontLabel.setText("Lähetettyjen pakettien reitit pyyhitty.");
    }

    
    private void createPackage() throws IOException {
        
        //creates new window for package creation; window needs to be closed to continue
        Stage creator = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("screen.fxml"));
        Scene scene = new Scene(page);
        creator.setScene(scene);
        creator.showAndWait();
        
        //updating storage interface
        packageBox.getItems().clear();
        packageBox.getItems().addAll(W.getStorage());
        
        //Updating garage log file
        log.setStuff(getStuffFromStorage());
    }

    //Writing log info into file. All send packages can be found from log file
    private String writePaper() {
        //Putting all info about already send packages to single string
        number = 0;
        line = "Lähetetyt paketit\n\n";
        
        //Deciding what to print
        if(list.size() == 0){
            line = "";
        }
        else{
        for(int i=0;i<list.size();i++){
            
                number = i+1;
                line += number + ".Paketti\n" + "Paketin lähtöpaikka: " + list.get(i).getStart() + "\n" + "Määränpää: " + list.get(i).getEnd() + "\n" + "Matkan pituus: " + 
                    list.get(i).getDistance() + "\n"
                    + "PakettiLuokka: " + list.get(i).getPrio() + "\n" + "Sisältö: " + list.get(i).getId() + "\n"
                    + "Paino: " + list.get(i).getPaino() + "\n" + "Koko: " + list.get(i).getLeveys()+ "x" + list.get(i).getPituus() + "x" + list.get(i).getSyvyys() + "\n";
                
                    //Adding info about packages condition depending these variables 
                    if(list.get(i).getPrio() == 2){
                     line += "Kunto: Ehjä\n\n\n";
                    }
                    else if(list.get(i).getPrio() == 1 && list.get(i).getBoolean() == false){
                        line += "Kunto: Ehjä\n\n\n";
                    }                    
                     else if(list.get(i).getId().equals("Timo")){
                         line += "Kunto: HAJALLA\n\n\n";    
                    }
                    else if(list.get(i).getBoolean() == false || list.get(i).getPaino() > 80){
                        line += "Kunto: Ehjä\n\n\n";
                    }
                    else{
                        line += "Kunto: Hajalla\n\n\n";
                    }
            
        }    
                    
        }
        //updating package balance
        log.setSendSaldo(number);
        return line;
    }
  
    private String getStuffFromStorage(){
        //Puttin all info into single string
        calculator = 0;
        word = "Varastossa olevat paketit\n\n";
        
        //Desiding what to print
        if(W.getStorage().size() == 0){
            word = "";
        }
        else{
            for(int i=0;i< W.getStorage().size();i++){

                     calculator = i+1;
                     word += calculator + ". Paketti\n" + "Paketin lähtöpaikka: " + W.getStorage().get(i).getStart() + "\n" + "Määränpää: " + W.getStorage().get(i).getEnd() + "\n" + "Matkan pituus: " + 
                         W.getStorage().get(i).getDistance() + "\n"
                         + "PakettiLuokka: " + W.getStorage().get(i).getPrio() + "\n" + "Sisältö: " + W.getStorage().get(i).getId() + "\n"
                         + "Paino: " + W.getStorage().get(i).getPaino() + "\n" + "Koko: " + W.getStorage().get(i).getLeveys()+ "x" + W.getStorage().get(i).getPituus() + "x" + W.getStorage().get(i).getSyvyys() + "\n"
                         + "\n\n";
            }
        }
        //Updating package balance
        log.setWareSaldo(calculator);
       return word;
    }

    @FXML
    private void makePackage(ActionEvent event) {
        //Action for make packages button witch launches new window
            try {
                createPackage();
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    @FXML
    private void openLog(ActionEvent event) throws IOException {
        //Action for button wicth launches window for showing current log state; Window has to be closed before contiuing
            Stage creator = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("LogScreen.fxml"));
            Scene scene = new Scene(page);
            creator.setScene(scene);
            creator.showAndWait();
    }
}
    
